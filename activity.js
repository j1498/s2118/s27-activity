let http = require("http");


let items = [
	{
		name: "Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name: "Horizon Forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 4000
	},
	{
		name: "Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}
];


http.createServer((req,res)=>{

	console.log(req.method);

	if(req.url === '/items' && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(items))
	}

	if(req.url === '/items' && req.method === "POST"){

		let requestBody = "";
		req.on('data', function(data){
			requestBody += data;
		})

		req.on('end', function(){
			requestBody = JSON.parse(requestBody);

		items.push(requestBody);

		res.writeHead(200,{'Content-Type': 'application/json'})
			res.end(JSON.stringify(items));

		})
	}



//Stretch Goal

    if (req.url === '/items/findItem' && req.method === "POST") {


        let requestBody = "";


        req.on('data', function(data) {

            requestBody += data;

        })


        req.on('end', function() {

            requestBody = JSON.parse(requestBody);

            items.push(requestBody);

            let itemFound = items.find(i => i.name === requestBody.name)

            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(itemFound));

        })

	}


}).listen(8000)

console.log(`Server is running on localhost:8000`);


